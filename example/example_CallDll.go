package main


/*
#cgo LDFLAGS: -ldl
#include <dlfcn.h>

double call_func(void* p, double a, double b) {
    double (*func)(double a, double b) = p;
    return func(a, b);
}
*/
import "C"
import "fmt"

func main() {

	handle := C.dlopen(C.CString("./Example_LvDLL.so"), C.RTLD_LAZY)
	if handle == nil {
		panic("Failed to open shared object.")
	}
	defer C.dlclose(handle)

	func_add := C.dlsym(handle, C.CString("Add"))
	if func_add == nil {
		panic("Could not found function pointer.")
	}

	var a, b float64 = 11.11, 22.2
	data := C.call_func(func_add, C.double(a), C.double(b)) // call LabVIEW DLL func

	fmt.Printf("data = %.2f\n", data)
}
