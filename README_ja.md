LabVIEWで作った .so(shared library)を Goから呼び出す
====

# 概要
使いみちの少ない LabVIEW for linuxだが、曲がりなりにも実行ファイルも.soも作れる。  
最近、勉強し始めたGo言語(以下、golang)から、LabVIEWで作成した.soを呼び出せるかどうか  
試してみた。  

golang (1.10)の cgoを使って、LabVIEW for linuxで作成した.soの関数を呼び出す。  

# テストした環境
* Core2Duo 搭載ノートPC(古!)
* Ubunutu 16 64bit
* LabVIEW 2018 for linux
* golang (1.10)

## 注意
golangでは、他のOSやCPU用のクロスコンパイルができる。  
今回作成に使った LabVIEW for linuxは、64bitのため、.soファイルも Linux 64bit x86専用。  
（バージョン2018あたりから、Linux用の32bit版LabVIEWはサポートされなくなっている）  
 そのため、作成される実行ファイルも.soも64bit x86専用。  
当然、LabVIEW Runtimeが入っている環境でないと動かない。  
RasPiあたりでも使えるとありがたいのだが、それはできない。  

# ソース

## LabVIEW側
関数例 Add
Add.vi ダイアグラム


.soファイルのビルド設定


## Go
  LabVIEWで作成したshared library 中の関数を呼び出す側のサンプル。

cgoを使うといっても、cgoで直接shared libraryに含まれている関数を呼び出すのではなく、  
<dlfcn.h>で定義されている shared libraryを扱う関数を使って、share library中の関数を  
呼び出している。以下のサンプルはWindowsでは使えない。Windows用にはDLLを扱う仕組みが  
別に用意されている。

golangでのサンプル
```
package main

/*
#cgo LDFLAGS: -ldl
#include <dlfcn.h>

double call_func(void* p, double a, double b) {
    double (*func)(double a, double b) = p;
    return func(a, b);
}
*/
import "C"
import "fmt"

func main() {

	handle := C.dlopen(C.CString("./Example_LvDLL.so"), C.RTLD_LAZY)
	if handle == nil {
		panic("Failed to open shared object.")
	}
	defer C.dlclose(handle)

	func_add := C.dlsym(handle, C.CString("Add"))
	if func_add == nil {
		panic("Could not found function pointer.")
	}

  var a, b float64 = 11.11, 22.2
	data := C.call_func(func_add, C.double(a), C.double(b)) // call LabVIEW DLL func

	fmt.Printf("data = %.2f\n", data)
}
```

# 参考

* [Go言語から共有ライブラリ(so)をロードして使う] (https://qiita.com/neko-neko/items/e6bcc9e307646241e226)
